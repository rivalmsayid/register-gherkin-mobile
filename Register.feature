Feature: Register

    @positive_case
    Scenario: TC.Reg.001.001-The user has successfully registered with valid information
        Given The user is already on the registration page
        When Run the Second Hand application
        And Click the Account menu
        And Click the Sign in button
        And Click Register
        And Fill in the registration form with valid information such as full name, email address, password, phone number, city, address
        And Click the Register button
        Then The user has successfully registers and is redirected to the account page

